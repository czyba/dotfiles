set folder = "~/mail/gmail"
set mbox_type = Maildir
set from = 'christopher.czyba@gmail.com'
set smtp_url = 'smtps://christopher.czyba@smtp.gmail.com'
set spoolfile = +INBOX
set record = "+[Gmail].Sent Mail"
set postponed = "+[Gmail].Drafts"
set trash="+[Gmail].Trash"

unmailboxes *
mailboxes "+INBOX" "+[Gmail].Drafts" "+[Gmail].Sent Mail" "+[Gmail].Trash" "+[Gmail].Spam"