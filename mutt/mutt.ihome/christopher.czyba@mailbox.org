set folder = "~/mail/mailbox"
set mbox_type = Maildir
set from = 'christopher.czyba@mailbox.org'
set smtp_url = 'smtps://christopher.czyba@mailbox.org@smtp.mailbox.org:465/'
set spoolfile = "+INBOX"
set record = "+Sent"
set postponed = "+Drafts"
set trash="+Trash"

unmailboxes *
mailboxes "+INBOX" "+Drafts" "+Sent" "+Trash" "+Junk"