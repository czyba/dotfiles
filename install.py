#!/usr/bin/env python3

import os
import shutil
import argparse

HOME_DIR = os.getenv('HOME')
MIMEAPPS = os.path.join(os.getcwd(), 'xdg', 'mimeapps.list')
DOTFILES_DIR = os.path.dirname(os.path.abspath(__file__))
HOME_SUFFIX = '.ihome'
CONFIG_SUFFIX = '.iconfig'
BASHRCD_SUFFIX = '.ibashrcd'
DESKTOP_SUFFIX = '.desktop'
LXQT_THEME_SUFFIX = '.ilxqt-theme'
XRESOURCES_SUFFIX = '.XResource'
SERVICE_SUFFIX = '.service'
TIMER_SUFFIX = '.timer'

CWD_STACK = []
INSTALL = 'install'
UNINSTALL = 'uninstall'
def gettargets():
    targets = []
    for filename in os.listdir(DOTFILES_DIR):
        path = os.path.join(DOTFILES_DIR, filename)
        if not os.path.isdir(path):
            continue
        if filename == '.git':
            continue
        targets.append(filename)
    targets.sort()
    return targets

PROGRAMS = gettargets()

parser = argparse.ArgumentParser(description='install and uninstall dotfiles')
subparsers = parser.add_subparsers(help='additional help', dest='subparser_name')
parse_install = subparsers.add_parser(INSTALL,help='install given dotfiles')
parse_uninstall = subparsers.add_parser(UNINSTALL, help='uninstall given dotfiles')

for tmp_parser in [parse_install, parse_uninstall]:
    tmp_parser.add_argument('-d', '--dry-run', action='store_true', dest='dry_run',
    help='print execution but do not execute')
    action = 'install' if tmp_parser == parse_install else 'uninstall'
    group = tmp_parser.add_argument_group(description='dotiles to ' + action + '. If none are supplied all are '+ action + 'ed')
    for program in PROGRAMS:
        group.add_argument('--' + program, action='append_const', dest='programs', const=program,
        help=action + ' ' + program + ' dotfiles')

args = parser.parse_args()

if args.subparser_name is None:
    print('error: requires at either install or uninstall to be chosen')
    quit()

chosen_args = args.programs if args.programs else PROGRAMS

def link(mfrom, target):
    if args.dry_run:
        print('ln -s ' + mfrom + ' ' + target)
        return
    filename = os.path.basename(target)
    dirname = os.path.dirname(target)
    if dirname == '':
        return
    if not os.path.isdir(dirname):
        os.makedirs(dirname)
    olddir = os.getcwd()
    os.chdir(dirname)
    os.symlink(mfrom, filename)
    os.chdir(olddir)

def unlink(target):
    if args.dry_run:
        print('rm -r ' + target)
        return
    if not os.path.exists(target) and not os.path.islink(target):
        return
    if os.path.isdir(target) and not os.path.islink(target):
        shutil.rmtree(target)
    else:
        os.unlink(target)

def choseAction(mfrom, target):
    if args.subparser_name == INSTALL:
        link(mfrom, target)
    elif args.subparser_name == UNINSTALL:
        unlink(target)

def choseHome(mfrom):
    basename = os.path.basename(mfrom)
    basename = basename[:-len(HOME_SUFFIX)]
    target = os.path.join(HOME_DIR, '.' + basename)
    choseAction(mfrom, target)

def choseConfig(mfrom):
    basename = os.path.basename(mfrom)
    basename = basename[:-len(CONFIG_SUFFIX)]
    target = os.path.join(HOME_DIR, '.config', basename)
    choseAction(mfrom, target)

def choseBashrcd(mfrom):
    basename = os.path.basename(mfrom)
    basename = basename[:-len(BASHRCD_SUFFIX)]
    target = os.path.join(DOTFILES_DIR, 'bash', 'bashrc.d.ihome' ,basename)
    choseAction(mfrom, target)

def choseDesktop(mfrom):
    basename = os.path.basename(mfrom)
    target = os.path.join(HOME_DIR, '.local', 'share', 'applications' ,basename)
    choseAction(mfrom, target)

def choseXResource(mfrom):
    basename = os.path.basename(mfrom)
    basename = basename[:-len(XRESOURCES_SUFFIX)]
    target = os.path.join(HOME_DIR, '.Xresources.d', basename)
    choseAction(mfrom, target)

def choselxqtTheme(mfrom):
    basename = os.path.basename(mfrom)
    basename = basename[:-len(LXQT_THEME_SUFFIX)]
    target = os.path.join(HOME_DIR, '.local', 'share', 'lxqt', 'themes',basename)
    choseAction(mfrom, target)

def choseSystemdServiceOrTimer(mfrom):
    basename = os.path.basename(mfrom)
    target = os.path.join(HOME_DIR, '.config', 'systemd', 'user', basename)
    choseAction(mfrom, target)

def checkPath(path):
    if path.endswith(HOME_SUFFIX):
        choseHome(path)
    elif path.endswith(CONFIG_SUFFIX):
        choseConfig(path)
    elif path.endswith(BASHRCD_SUFFIX):
        choseBashrcd(path)
    elif path.endswith(DESKTOP_SUFFIX):
        choseDesktop(path)
    elif path.endswith(XRESOURCES_SUFFIX):
        choseXResource(path)
    elif path.endswith(LXQT_THEME_SUFFIX):
        choselxqtTheme(path)
    elif path.endswith(SERVICE_SUFFIX) or path.endswith(TIMER_SUFFIX):
        choseSystemdServiceOrTimer(path)

for subdir in chosen_args:
    subdir_path = os.path.join(DOTFILES_DIR, subdir)
    for root, subdirs, files in os.walk(subdir_path):
        for sd in subdirs:
            path = os.path.join(root, sd)
            checkPath(path)
        for fn in files:
            path = os.path.join(root, fn)
            checkPath(path)
