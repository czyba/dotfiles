#/usr/bin/env bash

if [ "${XDG_SESSION_DESKTOP}" == "awesome" ]; then
    echo "ncmpcppcallback()" | awesome-client;
elif hash notify-send 2> /dev/null; then
    notify-send -t 5000 -a "" -u low "Music Player Daemon" "Song changed to $( ncmpcpp --quiet --current-song '{%a - }{%f}' )";
fi