#!/usr/bin/env python3
import json
from subprocess import run, PIPE
from sys import argv

completed_process = run(["i3-msg", "-t", "get_workspaces"], stdout=PIPE)

i3_msg = json.loads(completed_process.stdout)

for entry in i3_msg:
    if not entry["focused"]:
        continue
    name = entry["name"]
    parts = name.split(".")
    new_ws = parts[0] + "." + argv[1]
    completed_process = run(["i3-msg", "workspace", new_ws])
    break
