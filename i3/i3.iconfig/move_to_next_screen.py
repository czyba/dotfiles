#!/usr/bin/env python3
import json
from subprocess import run, PIPE
from sys import argv

completed_process = run(["i3-msg", "-t", "get_workspaces"], stdout=PIPE)

i3_msg = json.loads(completed_process.stdout)

workspace_map = {}
current_workspace = None
max_workspace = -1

for entry in i3_msg:
    if not entry["visible"]:
        continue
    name = entry["name"]
    parts = name.split(".")
    workspace_map[parts[0]] = name
    if max_workspace <= int(parts[0]):
        max_workspace = int(parts[0]) + 1
    if entry["focused"]:
        current_workspace = int(parts[0])

if current_workspace is None:
    exit(1)

num_active_workspaces = len(workspace_map)
if num_active_workspaces == 1:
    exit(1)

increment = int(argv[1])
index = current_workspace + increment
index = index % max_workspace
while index != current_workspace:
    if str(index) in workspace_map:
        new_ws = workspace_map[str(index)]
        run(["i3-msg", "workspace", new_ws])
        exit(0)
    index = index + increment
    index = index % max_workspace
