#!/usr/bin/env bash

function run {
  if ! pgrep $1 ;
  then
    "$@" &
  fi
}

if [ -d ~/.Xresources.d ]; then
    for i in ~/.Xresources.d/*.conf; do
        xrdb -merge "$i"
    done
fi

hash mpd 2> /dev/null && mpd
hash copyq 2>/dev/null && run copyq
hash urxvtd 2>/dev/null && run urxvtd -q
hash xss-lock 2>/dev/null && hash slock 2>/dev/null && run xss-lock -- slock
#hash compton 2>/dev/null && run compton -b --unredir-if-possible --backend=glx
hash nm-applet 2>/dev/null && run nm-applet
hash pasystray 2>/dev/null && run pasystray
#hash lxqt-notificationd 2> /dev/null && run lxqt-notificationd
hash dunst 2> /dev/null && run dunst
