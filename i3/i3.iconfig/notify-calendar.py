#!/usr/bin/env python3

import calendar
from subprocess import run, PIPE
from sys import argv
from datetime import date
from pathlib import Path


def printCalendar(year, month):
    currenMonth = calendar.month(year, month)
    title, currenMonth = currenMonth.split('\n', 1)
    currenMonth = currenMonth.replace('\n', "<br/>")
    linecount = currenMonth.count("<br/>")
    while currenMonth.endswith("<br/>"):
        currenMonth = currenMonth[0: currenMonth.rfind("<br/>")]
    currenMonth = currenMonth + ("<br/>" * (7 - linecount))
    today = date.today()
    if month == today.month and year == today.year:
        day = today.day
        if day < 10:
            currenMonth = currenMonth.replace(" " + str(day) + " ", ' <span foreground="red">' + str(day) + "</span> ")
            currenMonth = currenMonth.replace(" " + str(day) + "<", ' <span foreground="red">' + str(day) + "</span><")
            currenMonth = currenMonth.replace(">" + str(day) + " ", '><span foreground="red">' + str(day) + "</span> ")
        if day >= 10:
            currenMonth = currenMonth.replace(" " + str(day), ' <span foreground="red">' + str(day) + "</span>")
            currenMonth = currenMonth.replace(">" + str(day), '><span foreground="red">' + str(day) + "</span>")
    currenMonth = "<html>" + currenMonth + "</html>"
    home = str(Path.home())
    run([ home + "/.config/i3/notify-send.sh", "-t", "0", "--replace-file=/tmp/notification-widget", title, currenMonth])
    with open("/tmp/notification-widget-state", 'w') as f:
        f.write(str(month) + "\n" + str(year))

if argv[1] == "--current":
    today = date.today()
    printCalendar(today.year, today.month)
    exit(0)

with open("/tmp/notification-widget-state", 'r') as f:
    month = int(f.readline())
    year = int(f.readline())

month = month + int(argv[1])
if month > 12:
    month = month % 12
    year = year + 1
if month <= 0:
    month = (month + 12) % 13
    year = year - 1

printCalendar(year, month)
