# Dotfiles

This repository contains my dotfiles and a couple of icons.

In order to install the dotfiles use the `install.py` script and call install with the respective subprograms you want to install, i.e. if you only want to install the bash files call `./install.py install --bash`.

Icons are by Yusuke Kamiyamane. Licensed under a Creative Commons Attribution 3.0 License. You can take a look of other work [here]( http://p.yusukekamiyamane.com/ ).
